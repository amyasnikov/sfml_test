
#include <iostream>

#include <SFML/Graphics.hpp>

struct sfml_context {
  // struct rect_t {
  //   size_t     x;
  //   size_t     y;
  //   uint32_t   color;
  // };

  std::string name = "amyasnikov.pro";
  bool       need_recreate_window{true};

  size_t     g_x{1000};
  size_t     g_y{1000};
  float      v_x{(float) g_x};
  float      v_y{(float) g_y};
  sf::VertexBuffer quads;
  float      zoom{1};
  float      pos_offset_x{};
  float      pos_offset_y{};

  void rects_init() {
    quads = sf::VertexBuffer(sf::Quads);
    quads.create(4 * g_x * g_y);

    for (size_t i{}; i < quads.getVertexCount(); i += 4) {
      size_t x = (i / 4) % g_x;
      size_t y = (i / 4) / g_x;
      rects_update(x, y);
    }
  }

  void rects_update(size_t x, size_t y) {
    static size_t rnd = 17;
    rnd = (rnd ^ (rnd >> 16)) * 117 + 13;
    uint32_t color = (rnd << 8) | 0xFF;

    std::array<sf::Vertex, 4> rect = {
      sf::Vertex(sf::Vector2f(x + 0, y + 0), sf::Color(color)),
      sf::Vertex(sf::Vector2f(x + 0, y + 1), sf::Color(color)),
      sf::Vertex(sf::Vector2f(x + 1, y + 1), sf::Color(color)),
      sf::Vertex(sf::Vector2f(x + 1, y + 0), sf::Color(color)),
    };

    quads.update(rect.data(), rect.size(), 4 * (y + x * g_x));
  }

  void update_size() {
    float kv = 1. * sf::VideoMode::getDesktopMode().width / sf::VideoMode::getDesktopMode().height;
    float kg = 1. * g_x / g_y;

    if (kv > kg) {
      v_x = g_x * kv / kg;
      v_y = g_y * 1;
    } else {
      v_x = g_x * 1;
      v_y = g_y * kg / kv;
    }
  }

};

void draw_view(sfml_context& ctx, sf::RenderWindow& window) {
  auto view = window.getView();

  window.draw(ctx.quads);
}

void draw(sfml_context& ctx, sf::RenderWindow& window) {
  // std::cout << __PRETTY_FUNCTION__ << " #" << __LINE__ << std::endl;

  sf::View view(sf::FloatRect(0.f, 0.f, ctx.v_x, ctx.v_y));
  view.zoom(ctx.zoom);
  view.move(ctx.pos_offset_x, ctx.pos_offset_y);
  window.setView(view);

  draw_view(ctx, window);

  sf::View view_minimap(sf::FloatRect(0.f, 0.f, ctx.v_x, ctx.v_y));
  view_minimap.setViewport(sf::FloatRect(0.f, 0.f, 0.25f, 0.25f));
  window.setView(view_minimap);

  draw_view(ctx, window);

  window.setView(view);

  // std::cout << __PRETTY_FUNCTION__ << " #" << __LINE__ << std::endl;
}

int main() {
  sfml_context ctx;

  sf::RenderWindow window(sf::VideoMode::getDesktopMode(), ctx.name, sf::Style::Fullscreen);
  window.setFramerateLimit(10);

  ctx.rects_init();
  ctx.update_size();

  bool moving = false;
  sf::Vector2f pos_mouse;
  sf::Vector2f pos_old;

  while (window.isOpen())
  {
    sf::Event event;
    while (window.pollEvent(event))
    {
      switch (event.type) {
        case sf::Event::Closed: {
          window.close();
          break;

        } case sf::Event::TextEntered: {
          break;

        } case sf::Event::KeyPressed: {
          if (event.key.code == sf::Keyboard::Q) {
            window.close();
          } else if (event.key.code == sf::Keyboard::Add) {
            ctx.zoom /= 1.1;
          } else if (event.key.code == sf::Keyboard::Subtract) {
            ctx.zoom *= 1.1;
          } else if (event.key.code == sf::Keyboard::Left) {
            ctx.pos_offset_x -= ctx.zoom * 0.10 * ctx.v_x;
          } else if (event.key.code == sf::Keyboard::Right) {
            ctx.pos_offset_x += ctx.zoom * 0.10 * ctx.v_x;
          } else if (event.key.code == sf::Keyboard::Up) {
            ctx.pos_offset_y -= ctx.zoom * 0.10 * ctx.v_y;
          } else if (event.key.code == sf::Keyboard::Down) {
            ctx.pos_offset_y += ctx.zoom * 0.10 * ctx.v_y;
          }
          break;

        } case sf::Event::MouseWheelScrolled: {
          if (moving)
            break;

          if (event.mouseWheelScroll.delta > 0) {
            ctx.zoom /= 1.1;
          } else {
            ctx.zoom *= 1.1;
          }

          break;

        } case sf::Event::MouseButtonPressed: {
          if (event.mouseButton.button == sf::Mouse::Left) {
            sf::Vector2i pixelPos = sf::Mouse::getPosition(window);
            sf::Vector2f worldPos = window.mapPixelToCoords(pixelPos);
            ctx.rects_update(worldPos.x, worldPos.y);
          }

          if (event.mouseButton.button == sf::Mouse::Right) {
            moving = true;
            pos_old = window.mapPixelToCoords(sf::Vector2i(event.mouseButton.x, event.mouseButton.y));
          }
          break;

        } case sf::Event::MouseButtonReleased: {
          if (event.mouseButton.button == sf::Mouse::Right) {
            moving = false;
          }
          break;

        } case sf::Event::MouseMoved: {
          pos_mouse = window.mapPixelToCoords(sf::Vector2i(event.mouseMove.x, event.mouseMove.y));

          if (!moving)
            break;

          sf::Vector2i pixelPos = sf::Mouse::getPosition(window);
          sf::Vector2f worldPos = window.mapPixelToCoords(pixelPos);

          const sf::Vector2f pos_new = window.mapPixelToCoords(sf::Vector2i(event.mouseMove.x, event.mouseMove.y));
          const sf::Vector2f pos_delta = pos_old - pos_new;
          pos_old = window.mapPixelToCoords(sf::Vector2i(event.mouseMove.x, event.mouseMove.y));

          ctx.pos_offset_x += pos_delta.x;
          ctx.pos_offset_y += pos_delta.y;
          break;

        } default: {
          // std::cout << "type: " << event.type << std::endl;
        }
      }
    }

    window.clear();
    draw(ctx, window);
    window.display();
  }

  return 0;
}


